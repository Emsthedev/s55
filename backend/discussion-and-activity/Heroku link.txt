
ATTENDANCE: 09/13/2022
====================
1. Yoby Fernandez
2. Marnel Justine Brobio
3. Ronel Tayawa
4. Glenn Perey
5. Loren Dela Cruz
6. Red Dela Cruz
7. Maica Ocampo
8. Emmanuel Garcia
9. Nessa Agustin
10. Jhon Paul Galela
11. Alexis Vinasoy
12. Ruby Mae Sargento
13. Jay Leizl Reyes
14. Eli Raphael Del Rosario
15. Elysha Dumpit
16. Gabriel Guillermo
17. Jean Victoria Eraya
18. Joshua Pascua
19. Louies Aronn Dela Cerna



// process.env.PORT || 4000 means: whatever is in the environment(Heroku) variable PORT, or 4000 if there's nothing there.

process.env.PORT ||

Heroku App Link:
https://boiling-beach-38344.herokuapp.com/ | https://git.heroku.com/boiling-beach-38344.git
eraya ocampo, sargento, dumpit

1. Create project directory named s42-s46. Navigate to this directory and initialize npm via the terminal command npm init.


- you can hit enter on all prompts that will show up to set them to their defaults


2. Install the npm packages that we will need for this project:

- bcrypt - for hashing of user-submitted passwords

- cors - for allowing cross-origin resource sharing

- express - our framework of choice for Node.js

- jsonwebtoken - provides helper methods in working with JWT's

- mongoose - for mapping our JS objects to MongoDB documents


3. Create the following subdirectories and files in our project directory:


* controllers (directory)

	-productController.js (file)
	-userController.js (file)


* models (directory)

	-product.js (file)
	-user.js (file)


* routes (directory)

	-productRoutes.js (file)
	-userRoutes.js (file)


* .gitignore (file)
* app.js (file)
* auth.js (file)



4. Put node_modules in our .gitignore file to exclude the node modules of the packages we install in our git commits.


5. Create your server setup in app.js. (Express setup, Mongoose connection, Cors)


Eli - Wala pong totalAmounts and quantity na field. Naclone lang yung course booking wala pa masyado updates.

Marnel - No Authentication yes sa login wala pa registration. very detailed yung User Model. Pro sufficienct details. Cons kung magagamit ba talaga sya.


Dela Cerna - di pa tapos order models. clone routes and controllers no updates yet. 

Eraya - process.env.PORT in app.js, in Products model no orders field for array of objects.

Red DC - no auth.js yet, product model not done yet, no userRoutes yet, no usercontrollers yet

Yoby: process.env.PORT. no main URI yes in app.js . no userRoutes yet. no userControllers yet.

Galela: process.env.PORT. requried fields in User Model

Salman: product models not yet done.

Ocampo: no main URI yet at app.js(done). required fields in User Model. Wrong model. Combined embeded and ref model.

Pascua: No product model yet.the rest was just clone from course booking api

Perey - goods

Reyes - no auth.js

Sargento - process.env.PORT 

Tayawa -  no Products model yet unfinished. Not sure if embeded of referenced.


Del Rosario - missing , in order model line 32, registration controller function doesn't match the user model/schema fields


====================================================
Capstone Making Day 2 Deliverable:

Create a Product Sample Workflow:
1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API creates product using the contents of the request body.

Retrieve All Active Products Sample Workflow:
1. A GET request is sent to the /products endpoint.
2. API retrieves all active products and returns them in its response.


ATTENDANCE: 09/14/2022
====================
1. Eli Raphael Del Rosario
2. Jay Leizl Reyes
3. Maica Ocampo
4. Ronel Tayawa
5. Glenn Perey
6. Yoby Fernandez
7.Alexis Vinasoy 
8. Red Dela Cruz
9. Joshua Pascua
10. Marnel Justine Brobio
11. Alexis Vinasoy
12. Ruby Mae Sargento
13. Jay Leizl Reyes
14. Eli Raphael Del Rosario
15. Elysha Dumpit
16. Gabriel Guillermo
17. Jean Victoria Eraya
18. Joshua Pascua
19. Louies Aronn Dela Cerna
20. Ronel Andaya
21. Salman Gayampal
22. Loren Dela Cruz

checking:
Ms. Elysha - s41
Ms. Jay - s41
Sir Dela Cerna - 40

=====================================================
Capstone Making Day 3 Deliverables:

Retrieve Single Product Sample Workflow:
1. A GET request is sent to the /products/:productId endpoint.
2. API retrieves product that matches productId URL parameter and returns it in its response.

Update Product Sample Workflow:
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds product with ID matching the productId URL parameter and overwrites its info with those from the request body.

Archive Product Sample Workflow:
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.

Workflow:
1. User logs in, server will respond with JWT on successful authentication
2. JWT will be included in the header of the GET request to the /users/details endpoint
3. Server validates JWT
	- If valid, user details will be sent in the response
	- If invalid, deny request

ATTENDANCE: 09/15/2022
==============================
1. Marnel Justine Brobio
2. Gabriel Guillermo
3. Ronel tayawa
4. Ruby Mae Sargento
5. Red Dela Cruz
6. Jean Victoria Eraya
7. Louies Aronn Dela Cerna
8. Jesus Carullo
9.Jay Leizl Reyes
10. Emmanuel Garcia
11. Alexis Vinasoy
12. Glenn Perey
13. Joshua Pascua
14. Loren DEla Cruz
15 Maica Ocampo
16. Eli Raphael Del Rosario
17. Yoby Fernandez
18. Jhon Paul Galela
19. Ronel Andaya
20. Mark Ramos



Salman - no SS for update and archive products. No routes and controllers for update and archive yet.


=====================================================================
Capstone Making Day 4 Deliverables:
Non-admin User Checkout (Create Order) Workflow:
1. User logs in, server will respond with JWT on successful authentication
2. A POST request with the JWT in its header and the necessary fields in its request body will be sent to the /users/enroll (embedded) endpoint or /orders (referenced)
3. Server validates JWT
	- If valid, user will be able to place an order successfully.
	- If invalid, deny request

Retrieval of Authenticated User's Details Workflow:
1. User logs in, server will respond with JWT on successful authentication
2. JWT will be included in the header of the GET request to the /users/details endpoint
3. Server validates JWT
	- If valid, user details will be sent in the response
	- If invalid, deny request



ATTENDANCE: 09/16/2022
==============================
1. Yoby Fernandez
2. Jay Leizl Reyes
3. Ruby Mae Sargento
4. Loren Dela Cruz
5. Red Dela Cruz
6. Maica Ocampo
7. Elysha Dumpit
8.Emmanuel Garcia
9. Glenn Perey
10. Louies Aronn Dela Cerna
11. Alexis Vinasoy
12. Jesus Carullo
13. Jhon Paul Galela
14. Gabriel Guillermo
15. Marnel Justine Brobio
16. Salman Gayampal
17. Joshua Pascua
18. Ronel tayawa
19. Mark Ramos

/*
Heroku Deployment Instructions


Check if you have heroku CLI installed in your computer via the terminal command:


heroku --version


Register an account with Heroku via this URL:


https://signup.heroku.com/login


Create a file in the root directory of your project called Procfile with the following content:


web: node app


the file has to start with an uppercase P
the file should NOT have any file type extension
the spaces in the file content are


Login to Heroku via its CLI. Input the following in your terminal:


heroku login


you'll be redirected to the browser for authentication


Once logged in, create a heroku app with the following terminal command:


heroku create


must be done at project's root directory
an app name will be auto-generated for you
this will also automatically add a remote repository named heroku to your local repo, you can verify this with git remote -v


Get the application name of the newly created project


a free-tier Heroku account only allows for 5 projects at any given time


After committing changes, push your app to Heroku with the terminal command:


git push heroku master


heroku will build your application based on the contents of your package.json file
heroku will run your application based on the contents of your Procfile


To test your deployment, replace localhost:4000 with your Heroku app's URL in Postman

*/

No heroku links:
Carullo
Del Rosario
Eraya
Fernandez
Galela
Guillermo
Ramos
Reyes

