import { Accordion,Row,Col,Button,Card } from 'react-bootstrap';


export default function Highlights() {
    return(
     
        <Accordion defaultActiveKey="0" >
         <Accordion.Item eventKey="0">
            <Accordion.Header>Resources</Accordion.Header>
            <Accordion.Body>
    
                <Row className="mt-3 mb-3">
                    <Col xs={12} md={4}>
                        <Card className="cardHighlight p-3">
                            <Card.Body>
                                <Card.Title>What is Cassandra?</Card.Title>
                            
                                <Card.Text>
                                Cassandra is one of the most efficient and widely-used NoSQL databases. 
                                One of the key benefits of this system...
                    
                                </Card.Text>
                                <Button variant="info" href='https://www.bmc.com/blogs/apache-cassandra-introduction/'>Read More</Button>
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col xs={12} md={4}>
                        <Card className="cardHighlight p-3">
                            <Card.Body>
                                <Card.Title>Top 3 benefits of using Cassandra</Card.Title>
                            
                                <Card.Text>
                                Fast, scalable and reliable – Cassandra can modernise your cloud

                                </Card.Text>
                                <Button variant="info" href='https://ubuntu.com/blog/apache-cassandra-top-benefits#:~:text=One%20open%20source%20application%2C%20Apache,Cassandra%20for%20mission%2Dcritical%20features.'>Read More</Button>
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col xs={12} md={4}>
                        <Card className="cardHighlight p-3">
                            <Card.Body>
                                <Card.Title>Companies that uses Cassandra</Card.Title>
                            
                                <Card.Text>
                                From startups to the largest enterprises, the world runs on Cassandra.
                                </Card.Text>
                                <Button variant="info" href='https://cassandra.apache.org/_/case-studies.html'>Read More</Button>

                            </Card.Body>
                        </Card>
                    </Col>
                </Row>




            </Accordion.Body> 
      </Accordion.Item> 
    
    </Accordion> 



       
    )
};