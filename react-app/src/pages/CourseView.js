import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView(){
	//hook for global state
	const { user } = useContext(UserContext);

	//allows us to gain access to methods that will 
	//allows us to redirect a user to a different page after enrolling  a course
	const navigate = useNavigate(); //useHistory

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	//useParams is a hook that allows us to receive the courseId passed via URL
	const {courseId} = useParams();

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
			method : 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if (data === true) {
				Swal.fire({
					title: 'Successfully enrolled!' ,
					icon: 'success',
					text: 'Thank you for enrolling'
				});

					navigate("/courses");


			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});
			};
		});
	};

	useEffect(() => {
		console.log(courseId)
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [courseId]);
	
	return(
// put Row on view
		<Container className="mt-5">
		 <Row className="d-flex m-3">
		<Col xs={12} md={8} >
            <Card className="cardCourses p-3" >
                <Card.Body >
                   
                    <Card.Header className="text-center card-header">{name}</Card.Header>
                    <Card.Subtitle className="mt-4 text-">Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Subtitle>Class Schedule:</Card.Subtitle>
                    <Card.Text>5:30PM - 9:30PM</Card.Text>
                    { user.id !== null ?
						<Button variant="info" onClick={() => enroll(courseId)}>Enroll</Button>
						 :
						<Link className = "btn btn-danger" to="/login">Log In To Enroll</Link>
					}

                   
                </Card.Body>
            </Card>
        </Col>
		</Row>
		</Container>
	)
}