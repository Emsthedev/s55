import { Fragment,useState,useEffect,useContext } from 'react'
import { Form, Button,Stack } from 'react-bootstrap'
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

    //destructing of userContext they are a variable here
    //allows us to consume the UserContext
    //object and its properties to be used in validation
    const {user, setUser } = useContext(UserContext);
  
  //state hook
  const[email, setEmail] = useState(""); 
  const [password,setPassword] = useState("");
  

  
  //state to determine whether submit button is enable or not
  const [isActive, setIsActive] = useState(false);


//Check if values are successfully passed

//   console.log(`Email: ${email}`);
//   console.log(`Password: ${password}`);


//Authenticates user if existing in local storage/db 
  function loginUser(e) {
   
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email : email,
                password: password
            })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        console.log(data.access);


        if(typeof data.access !== "undefined") {
            localStorage.setItem('token', data.access)
            retrieveUserDetails(data.access)

             Swal.fire({
                title: "Login Successful",
                icon: "success",
                text: "Welcome to Zuitt!"
            }) 
        } else {
            Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Please check your login details and try again"
            }) 
        }
    })

    //setItem stores data in localStorage
    //             ("for mongoDB", from statehook)
    // localStorage.setItem("email", email);

    // setUser({
    //     email: localStorage.getItem("email")
    // })

    // clear input fields
    setEmail('');
    setPassword('');
 
    // console.log(`${email} has been verified! Welcome Back!`);
    // alert(`Hi! ${email} you have successfully logged in !`);
};


//RetrieveUserDetails

const retrieveUserDetails = (token) => {
    //fetch request user detatils
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${ token }`
        }
    }) //fetch
    .then(res => res.json())
    .then(data => {
        console.log(data);
        //Global state
        setUser({
            id: data._id,
            isAdmin: data.isAdmin
        }) //setuser
    })//.then data
}


  //useEffect for button
  useEffect(() => {
    if (email !== '' && password !== ''){
        setIsActive(true);
    } else {
        setIsActive(false);
    }
}, [email,password]);



  
  
  
  
    return (
     
        (user.id !== null ) ?
		<Navigate to ="/courses"/>
	:
  
        <Stack gap={2} className="col-md-5 mx-auto">
        <Fragment>

            <h1 className='text-center mt-3'>Login Page</h1>
            
            <Form onSubmit={(e) => {loginUser(e)}}>

            <Form.Group className="mb-3" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                            type="email"
                            value = {email}
                            placeholder="Enter your email here"
                            onChange = {(e) => {setEmail(e.target.value)}}
                            required
                        />

                <Form.Text className="text-muted">
                We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                            type="password"
                            value = {password}
                            placeholder="Enter your Password"
                            onChange = {(e) => {setPassword(e.target.value)}}
                            required
                        />
            </Form.Group>

            
            { isActive ?
                <Button variant="success" type='submit' id='submitBtn' >
                Submit
            </Button>
                :
                <Button variant="secondary" type='submit' id='submitBtn' disabled>
                Submit
            </Button>

            }
            
            </Form>
        </Fragment>
        </Stack>



  )
}